package com.cynergy.studentlist

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.cynergy.studentlist.Model.Student
import com.cynergy.studentlist.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private var students = mutableListOf<Student>()

    private val adaptador : StudentAdapter by lazy {
        StudentAdapter(students)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //setContentView(R.layout.activity_main)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        loadData()
        setupAdapter()
    }

    private fun setupAdapter() {
        binding.rvStudents.adapter = adaptador
        binding.rvStudents.layoutManager = LinearLayoutManager(this)
    }

    private fun loadData() {
        students.apply {
            add(Student("Javier","Junior Developer", "javierDev@gmail.com"))
            add(Student("Marcos","Contadora", "Marcos@gmail.com"))
            add(Student("Liz","Asistente", "Liz@gmail.com"))
            add(Student("Jhon","Odontologa", "Jhon@gmail.com"))
            add(Student("Laura","Doctora", "Laura@gmail.com"))
            add(Student("Gina","Psicóloga", "Gina@gmail.com"))
            add(Student("Veronica","Administradora", "Veronica@gmail.com"))
        }
    }
}