package com.cynergy.studentlist.Model

data class Student(val name: String, val career: String, val email: String)