package com.cynergy.studentlist

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.cynergy.studentlist.Model.Student
import kotlinx.android.synthetic.main.item_student.view.*

class StudentAdapter(val students: MutableList<Student>): RecyclerView.Adapter<StudentAdapter.StudentAdapterViewHolder>(){

    inner class StudentAdapterViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        fun bind(student: Student){
            itemView.apply {
                tvPhoto.text = student.name.first().toString()
                tvName.text = student.name
                tvCareer.text = student.career
                tvEmail.text = student.email
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StudentAdapterViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_student,parent,false)
        return StudentAdapterViewHolder(view)
    }

    override fun onBindViewHolder(holder: StudentAdapterViewHolder, position: Int) {
        val student: Student = students[position]
        holder.bind(student)
    }

    override fun getItemCount(): Int {
        return students.size
    }

}